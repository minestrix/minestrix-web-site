function openTab(evt, tabName) {
    var i, x, tablinks, sec, text;

    x = document.getElementsByClassName("theme-card");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("theme-button");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" is-info", "");
    }

    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " is-info";

    sec = document.getElementById("theme-section");
    sec.classList.toggle("has-background-black-ter");

    text = document.getElementsByClassName("theme-text");
    for (i = 0; i < text.length; i++) {
        
        text[i].classList.toggle("has-text-white");
    }
}