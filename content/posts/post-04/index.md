---
title: "MinesTRIX Version 1.6.0"
date: 2022-08-26T23:43:07+02:00
draft: false
author: "henri2h"
topics: []
tags: [matrix, dev]
---

Hi all!

Quite a lot happened since the last time, so let's have a quick overview of what's new.

## 🔑 New login experience

First impression matter, for this we resigned the login experience to provide a more clean and good-looking UI. Also, we took the opportunity to enable single sign for desktop and mobile. Web stills needs to be done.

Watch out, it might be a little frenchy :)

{{< imgproc new_login Resize "800x" "New login page" />}}

## ✨ General home page improvements

The main focus in this release was the publishing experience system.

It's now more simple to write post, you can now embed multiple image in them. Also, if you have multiple profile, you can select the profile on which you want to publish your post while writing it.

**PS:** Did you notice? The navigation bar for desktop got some love. It's way better isn't it ;)


{{< imgproc home_page Resize "800x" "Some work on home page" />}}

## 📷 New post gallery view

We wanted to allow commenting each pictures individually, so, for this, we got back to our [MSC](https://github.com/matrix-org/matrix-spec-proposals/pull/3639) and modified it to send each picture as a different event and then making a link to them in the post. With this in place, it is now possible to do so. 

{{< imgproc new_gallery_view Resize "800x" "New login page" />}}

## 👤 User page

For better navigation, we added a tab view to switch between the posts sends on a feed, the follower of this particular feed and a tab displaying all the pictures sent in this particular room.

Using an upcoming MSC (profile as space) it's now possible to discover the feeds of other MinesTRIX's users.For this, the user needs to advertise it's feed in his profile as space. Then, you can send follow request which is in fact a knocking request.

However, some issue prevents us to get the room hierachy over federation, so you cannot discover the feed from users not on the same server. You need to join the profile space first to be able to do this.

{{< imgproc user_page Resize "800x" "Brand new user page" />}}

## 👥 Group page

The group page also received some improvement in terms of layout. It also now properly request new post when scrolling through history, allow you to invite users and give some basic settings for this room.

{{< imgproc group_page Resize "800x" "New group page" />}}

## 📱Multi account support (thanks fluffychat 🎉)

We also took the opportunity to offload most of the client logic to the minestrix-chat SDK and it's now possible, thanks to the fluffychat implementation, to have multiple account and switch between them. For this, just go to the settings and go to switch account and add other accounts.

Taking screenshots with different accounts is now way simpler. So for this update, you have a lot of screenshots 🚀 

## 🤔 Don't forget the story (thanks fluffychat again 🎉)

Story are an important feature of a social media platform so we have some news for our story lovers also.

The story viewing experience was reworked thanks to the fluffychat implementation and it should be way smother and now. Also, as in fluffychat, it's now possible to view multiple stories from the same user. We also added support for some new story features,which we now have to add in the story editor. 

{{< imgproc stories_love Resize "800x" "Storiesss" />}}

## 💬 Chat settings got new features

We all love settings, don't we ? 

We added three new panels in the room settings:
* **Roles & permissions:** still a bit rough but at least should allow you to edit the chat permissions.
* **Security:** Yehaaa, you can now enable E2E encryption in chats, change the join rules and room visibility options.
* **Room media:** You now have a dedicated view showing all the media sent into this room. For this the app will paginate through the history to get all the media sent.

{{< imgproc new_chat_design Resize "800x" "The chat page updated" />}}


{{< imgproc chat_list_mobile Resize "300x" "Chat list on mobile" />}}

{{< imgproc chat_page_mobile Resize "300x" "Chat page on mobile" />}}

It's now also possible to display the active user with which you have a DM so you can reach out to them when they are available. Also, when opening a DM chat, the app display user info about the current user you are messaging. For example, it's displaying if the user is active, it's session information and the mutual rooms.

{{< imgproc active_user Resize "800x" "Active user on desktop" />}}


### 🔎 Displaying user info

As for DM room, we provide a way to display the information about each user by clicking on the user avatar next to the message or in the room participants list.

It's now displaying
* the user power level
* your mutual rooms
* it's different sessions and allow you to verify this particular user

{{< imgproc new_user_details Resize "300x" "User info" />}}

## ⚙️ App settings

Lastly, the room settings is now responsive and the experience should be better on desktop !

The routing library didn't want us to do it, so we found a bit of a messy hack, but hey, it works! Isn't it beautiful now ?

Also, the settings list is now adaptive and should give a native feel to Android and iOS users. We are using the [settings_ui](https://pub.dev/packages/settings_ui) package to provide this.

{{< imgproc new_settings_page Resize "800x" "New adaptive settings page" />}}

## 💣 Beta feature: Calendar event support

It's now possible to organize events through MinesTRIX, for this, you create a room, invite the person you want, and they will be able to say if they come or not through the event poll. As a group, you can send post and messages in the same room for better organisation.

Also, the place and date information are easily editable for easier retrieval.

{{< imgproc calendar_page Resize "800x" "Event page" />}}


## 📩 Packaging

Finally, we wanted to bring snap and F-Droid support, but both of them refuse to work for whatever reason. They did win for this time. But hey, I won't let them continue to this.

If you want to help the project and now snap packaging, could you figure out why the snap building is failing ? :D Another idea would be to switch to a Flatpak distribution. Same issue, need to find time to do it.


That's all for today, see you next time ;)