---
title: "Nov 26 Update"
date: 2021-11-26T00:45:51+01:00
draft: false
author: "henri2h"
topics: []
tags: [matrix, dev]
---

Hi all!!

Quick update on [MinesTRIX](https://minestrix.henri2h.fr/) (a privacy focused social media based on MATRIX).
This week was focused on performance and stability.

* Changed database to use Fluffybox, this should greatly improve performances on web (thanks Famedly !)
* Scrolling through the posts of a profile now properly request history.
* Friend suggestions are now sorted according to the sum of user appearance in all rooms. Naïve, but it's the first step.
* Chat page has also been redesigned. Now support replies and reactions. Chat settings now display room avatar and fetch user list from server.
* Bug affecting MinesTRIX profile creation has been fixed. Login process should be way more stable now.
* MinesTRIX rooms sync has been rewritten to take into account sync events to rebuild the list.
* Debug page now allow forcing sorting rooms.
* Various post display enhancement (links are now clickable, thanks kellya!)

[Come chat with us !](https://matrix.to/#/#minestrix:carnot.cc)
