---
title: "TWIM Project announcement"
date: 2021-11-19T00:45:51+01:00
draft: falsew
author: "henri2h"
topics: []
tags: [matrix, dev]
---

Hi all!!

Today I want to showcase you MinesTRIX. MinesTRIX is a decentralized social media based on matrix.
The goal is to create a privacy respectful social media using the power of matrix while trying to be as simple as possible.
## Two Objectives
- Showing that matrix could be used to build such a system.
- Helping find your friends using matrix
## Currently supported
- Posting
- Adding and accepting friends
- Basic post management
- Creating groups, posting and adding users to it
- E2EE device verification (thanks FluffyChat !!)
- Cross platform thanks to Flutter (Android, iOS, Linux, Windows, MacOS, WEB)
## Now what ?
- Stability fixes
- Finding a logo ;)
- Bring sharing functionality for public groups.
- Adding support for the Circle application.
- Enhance the friends' suggestion algorithm (Currently it's a really naïve one :D)
- Add reactions for chats and posts

[🚀 Demo](https://app.minestrix.henri2h.fr/)

[🏗️ Gitlab](https://gitlab.com/minestrix/minestrix-flutter)

[📱 Chat](https://matrix.to/#/!dDmzUleHWblVbYBPcf:carnot.cc?via=carnot.cc)
