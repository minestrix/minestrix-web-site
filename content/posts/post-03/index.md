---
title: "MinesTRIX Version 1.5.5"
date: 2022-04-22T19:31:22+02:00
draft: false
author: "henri2h"
topics: []
tags: [matrix, dev]
---
Hi!

just did release V.1.5.5 today, this is the last update since a few months ago.

{{< imgproc img_phone Resize "300x" "Phone view" />}}

Some of the changes are:

- **feat:** compliance to the latest version of [MSC3639: Matrix for the social media use case](https://github.com/matrix-org/matrix-spec-proposals/pull/3639)
- **feat:** added images support for posts. You can now send multiple images in a post and display them in a gallery view.
- **feat:** New emoji picker, you can now select and react to message in one continuous press.
- **feat:** Comments nesting, (using threads)
- **feat:** Image comment (read only :()
- **feat:** Dark theme support
- **feat:** New settings page (added a lab section :D, seems you need to have one if you want to build a proper Matrix client)
- **feat:** proper emoji support for desktop. Thanks fluffychat :)
- **feat:** Initial story support
- **feat:** various UI enhancements.

{{< imgproc img_desktop Resize "800x" "Desktop view" />}}

Concerning the messaging feature:
- **feat:** Initial poll support
- **feat:** read only spaces support
- **feat:** room list search box
- **feat:** chat settings dialog
- **feat:** read receipts list dialog
- **feat:** reaction list dialog
- **feat:** now use rounded avatar. Let's be consistent with other clients.
- **feat:** better display for replies and `m.notice`.
- **feat:** Localization support thanks to the Matrix Dart SDK


{{< imgproc img_chat Resize "300x" "The chat view" />}}

And some work already in for multiple profiles support, and profile as space (just use a public space to store your profile rooms).

If you are interested, come say hi in [the room](https://matrix.to/#/#minestrix:carnot.cc).

PS: You can also help us to design the underlying MSC, especially, we need some opinions on how to name things (the profile rooms) [the room](https://matrix.to/#/#social-matrix:carnot.cc) !!

[GitLab](https://gitlab.com/minestrix/minestrix-flutter)

